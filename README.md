# Thesaurus
Author : Andrey Zhalnin

## Преимущества управление инфраструктурой и конфигурацией:
- Весь код хранится в системе контроля версий (СКВ).
- Единый источник изменений - это СКВ.
- Использование практик разработки программного обеспечения (ПО).
- Повторяемость кода и конфигураций.

## Преимущества использования CI/CD подхода:
- Скорость вывода новой функциональности от запроса клиента до запуска в эксплуатацию.
- CI/CD позволяет запускать обновления за считанные дни или недели по сравнению с целым календарным годом при классическом waterfall-подходе. Новые сервисы – новые конкурентные бизнес-преимущества.
- Появляется возможность не просто воспроизводить функциональность решений конкурентов, но и значительно опережать их в разработке и внедрении новых инструментов.
- Возможность выбора оптимального варианта за счет оперативного тестирования и большего числа итераций.
- Качество итогового результата выше: автотестирование охватывает все аспекты продукта, что труднореализуемо при стандартном релизном подходе.
- Все ошибки и тонкие места выявляются и удаляются ещё на ранних этапах разработки.

## Pipeline CI/CD конвейера
![](https://gitlab.com/project2019-02/thesaurus/uploads/977f09185e43837eee1d1fd75941347a/image.png)

## Назначение:
- Подсистема хранения и управления нормативно-справочной информацией

## Возможности:
- CRUD HTTP Restful API
- Механизм обновления инстансов
- Импорт из CSV
- Статические справочники
- Пользовательские справочники
- Гибридные справочники

## Архитектура:
![](https://gitlab.com/project2019-02/thesaurus/uploads/fdedff14f81f63b69467a4ebb044767f/thesaurus-arch.png)
```
                                ____
              -----------      (    )
user 8-) ---> | MainAPP | ---> ( DB )
              -----------      (    )
                  ^             ~~~~
                  |
              ____|_____
              |  CSV   |
              | source |
              ~~~~~~~~~~
```
В качестве DB используется MongoDB.


## API Reference
- GET /status Статус инстанса
- GET /documents Коллекция документов
- POST /documents Создание документа
- PUT /documents Редактирование документа
- DELETE /documents Удаление документа

## Логирование
```
| Контекст | Уровень | Сообщение                                            | Описание                                               |
|----------|---------|------------------------------------------------------|--------------------------------------------------------|
| CORE     | INFO    | resource=DB addr=[MONGO_HOST:PORT] status=CONNECTED  | Соединение с БД успешно установлено                    |
| CORE     | FATAL   | resource=DB addr=[MONGO_HOST:PORT] status=FAILED     | Соединение с БД  не может быть установлено             |
| CORE     | DEBUG   |                                                      | Детализация ошибки подключения к БД                    |
| CORE     | INFO    | version=v[APP_VERSION] status=STARTED                | Приложение успешно запущено                            |
| DOCUMENT | INFO    | documentType=[DOC_TYPE] updateStatus=[UPDATE_STATUS] | Статус обновления справочника (UPDATING|UPDATED|ERROR) |
```

## Параметры запуска
```
-c [путь к файлу конфигурации]
--update - Обновление справочников
```
## Конфигурация
\* - обязательные параметры
_- значения по умолчанию
```

| Параметр             | Тип     | Описание                                                     | ENV                            |
|----------------------|---------|--------------------------------------------------------------|--------------------------------|
| *http.host           | string  | Адрес   бинд-хоста HTTP интерфейса (_0.0.0.0)                |  THESAURUS_HTTP_HOST           |
| http.port            | integer | Порт   бинд-хоста HTTP интерфейса (_80)                      |  THESAURUS_HTTP_PORT           |
| *db.host             | integer | Адрес   хоста БД                                             |  THESAURUS_DB_HOST             |
| db.port              | string  | Порт   хоста БД (_27017)                                     |  THESAURUS_DB_PORT             |
| *db.database         | string  | Название   БД                                                |  THESAURUS_DB_DATABASE         |
| db.login             | string  | Логин   пользователя БД                                      |  THESAURUS_DB_LOGIN            |
| db.password          | string  | Пароль   пользователя БД                                     |  THESAURUS_DB_PASSWORD         |
| csv.path             | string  | Путь до   папки со словарями                                 |  THESAURUS_CSV_PATH            |
| csv.separator.column | string  | Разделитель   полей CSV (,)                                  | THESAURUS_CSV_SEPARATOR_COLUMN |
| sentryDSN            | string  | DSN   аггрегатора ошибок                                     |  THESAURUS_SENTRY_DSN          |
| logging.output       | string  | Вывод   ошибок в _STDOUT или файл (указывается путь до файла)|  THESAURUS_LOGGING_OUTPUT      |
| logging.level        | string  | Уровень   логирования (DEBUG|ERROR|FATAL|_INFO)              |  THESAURUS_LOGGING_LEVEL       |
| logging.format       | string  | Формат   логов (_TEXT|JSON)                                  |  THESAURUS_LOGGING_FORMAT      |
```

## Справочники
Обязательный столбец `code`. Должен быть первым.
Пример:
```
code,text
1234,Info about sth
```

## Run
Run this command in project root:
```commandLine
docker-compose up -d
```

## Testing

- После запуска инфраструктуры, можно зайти по ссылке:
curl -sSLk -XGET 'http://<Instanse_IP>:8585/documents?locale=RUS&type=patient.genders' | jq .
```
Иногда по непонятным причинам коннект к приложению зависает. При этом контейнеры запущены. Помогает ручной перезапуск виртуалки.
```

### By `go test`
To run all tests recursively, run this in project root:
```commandLine
$ docker-compose run --rm --name thesaurus_test thesaurus sh -c 'CGO_ENABLED=0 go test -cover -covermode atomic ./...'
```

## Работу приложения можно посмотреть по запросам:
http://34.77.105.74:8585/documents?locale=RUS&type=va.sides
http://34.77.105.74:8585/documents?locale=RUS&type=patient.genders

## Логическая схема взаимодействия копонентов
![](https://gitlab.com/project2019-02/thesaurus/uploads/4ad486c9f802fec0b61526a31cad75c3/Thesaurus_architecture.png)

## Разворачивание инфраструктуры.
- Для разворачивания инфраструктуры используется связка Packer+Terraform. В качестве платформы выбран Google Cloud Platform.
- Инфраструктурный репозиторий https://gitlab.com/project2019-02/infra 
- В первую очередь создаётся базовый образ для всех серверов. Операционная система CentOS 7. Файлы для Packer находятся в папке `packer`. Шаблон базового образа в файле `centos7-base.json`. Переменные объявлены в вайле `variables.json`. Команда для запуска сборки базового образа `packer build -var-file=variables.json centos7-base.json`.
- Далее посредством Terraform разворачиваются продуктовые сервера: сервер приложения с базой данных и сервер мониторинга. Сетевая инфраструктура также разворачивается посредством terraform. Файлы для Terraform находятся в папке `terraform`. Файлы с описанием сервера приложений и базы данных находятся в папке `modules/app`. Файлы с описанием сервера мониторинга находятся в папке `modules/mon`. Файлы с описанием сетевой инфраструктуры находятся в папке `modules/vpc`.

## Преимущества быстрой обратной связи.
- Повышает качество продукта.
- Позволяет оперативно реагировать на значимые события/изменения.
- Повышается скорость обучения внутри организации.

## Мониторинг и алёртинг.
* Система мониторинга построена на связке Grafana+Prometheus. В качестве експортёров prometheus использовались следующие компоненты:
- [cAdvisor](https://github.com/google/cadvisor). Агент системы внутреннего мониторинга Kubernetes, собирающий метрики производительности и информацию об использовании контейнерами, работающими в пределах узла, таких ресурсов как время работы центрального процессора, оперативной памяти, нагрузку на файловую и сетевую системы.
- [node-exporter](https://github.com/prometheus/node_exporter). Сборщик параметров состояния серверов семейства *nix. Охватывает огромное колическтво метрик: cpu, ram, disks, nets и т.д.
- [MongoDB-exporter](https://hub.docker.com/r/eses/mongodb_exporter/). Возможности:
1. Метрики статуса сервера (cursors, operations, indexes, storage, и т.д.)
2. Метрики набора реплик (members, ping, replication lag, и т.д.)
3. Oplog метрики реплик (size, length in time, и т.д.)
4. Метрики шард (shards, chunks, db/collections, balancer operations)
5. Метрики движка хранилища RocksDB (levels, compactions, cache usage, i/o rates, и т.д.)
6. Метрики движка хранилища WiredTiger (cache, blockmanger, tickets, и т.д.).
- Для доступа к дашборду Grafana пройти по [ссылке](http://34.77.11.243:3000/). Логин: user, пароль: user.
* Алёртинг реализован посредством [alertmanager](https://prometheus.io/docs/alerting/alertmanager/). Обрабатывает предупреждения, отправляемые клиентскими приложениями, такими как сервер Prometheus. Он заботится о дедупликации, группировании и маршрутизации их для правильной интеграции получателя, такой как электронная почта, PagerDuty или OpsGenie.
- В рамках настоящего проекта реализован алёртинг при падении основного приложения `thesaurus`. Оповещения присылаются в канал slack.
